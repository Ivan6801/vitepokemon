import React from "react";
import {
  Header,
  Spinner,
  ContentPage,
  PokemonList,
  Footer,
} from "../components";

import { useData } from "../hooks/useData";

export function Home() {
  const { isLoading, pokemonDataList } = useData();

  return (
    <main className="min-h-screen bg-gray-100 transition-colors duration-500 dark:bg-gray-900">
      <Header />

      {isLoading && <Spinner />}
      <ContentPage>
        <PokemonList pokemonDataList={pokemonDataList} />
      </ContentPage>

      <Footer />
    </main>
  );
}
