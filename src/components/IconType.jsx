/* eslint-disable react/prop-types */
export function IconType({ letter }) {
  return <span style={{ fontFamily: 'Essentiarum' }}>{letter}</span>;
}
