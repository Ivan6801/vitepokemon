/* eslint-disable import/no-unresolved */
/* eslint-disable react/prop-types */
/* eslint-disable react/jsx-props-no-spreading */
import { usePagination } from "../hooks/usePagination";
import { PaginationComponent } from "./PaginationComponent";

export function ContentPage({ children }) {
  const props = usePagination();

  return (
    <>
      <PaginationComponent {...props} />
      {children}
      <PaginationComponent {...props} />
    </>
  );
}
