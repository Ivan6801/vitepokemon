import React from "react";
import "./spinner.css";

export function Spinner() {
  return (
    <div className="wrapper z-20 scale-105 bg-black/50">
      <div className="pokeball" />
    </div>
  );
}
